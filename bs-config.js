module.exports = {
  open: false,
  ghostMode: false,

  server: {
    baseDir: 'static'
  },

  files: [
    'static/**/*.{html,js,css,png,jpg,jpeg,gif,svg}'
  ]
};

